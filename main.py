import logging 
import psycopg2

log = logging.getLogger()
log.setLevel(logging.INFO)

DB_NAME = "lm"
DB_HOST = "db-lm.vscloud.local"
DB_USER= "lambda_role"
DB_PASS = "iQWWaz5o"

QUERY = "insert into lmadmin.lambda_executions(context_name, start_date) values ('%s', now()) on conflict(context_name) do update set start_date = EXCLUDED.start_date;"


def handler(event, context):           
    try:        
        context_name=event['context_name']
        log.info(f"Invoked for context {context_name}")
        conn = psycopg2.connect(database=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)
        cur = conn.cursor()
        cur.execute(QUERY % context_name)
        conn.commit()
        conn.close()
        log.info("OK")
    except Exception as e:
        log.error(e)

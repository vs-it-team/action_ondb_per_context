LAMBDA_NAME = action_ondb_per_context

clean:
	rm -rf main.zip
zipproject:
	zip -g main.zip main.py

dep:
	cd package && zip -r9 ../main.zip .

zipsql:
	zip -g main.zip *sql

zip: clean dep zipproject

push:
	aws lambda update-function-code --function-name=$(LAMBDA_NAME) --zip-file=fileb://main.zip

#updateenv:
#	aws lambda update-function-configuration --function-name $(LAMBDA_NAME) --environment "Variables={APPLICATION_NAMES=$(APPLICATIONS), CONNECTION_STATE=$(CONNECTION_STATE), INTERVAL=$(INTERVAL)"	


deploy: cleandep installdep zip push

cleandep:
	rm -rf package

installdep:
	pip install -r requirements.txt --target package
	
